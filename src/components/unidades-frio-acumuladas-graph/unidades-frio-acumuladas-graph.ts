import { Component, Input, ViewChild, ElementRef } from '@angular/core';
import { Service } from '../../models/service';
import Chart from 'chart.js';
import moment from 'moment';
import * as _ from 'lodash';

/**
 * Generated class for the UnidadesFrioAcumuladasGraphComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'unidades-frio-acumuladas-graph',
  templateUrl: 'unidades-frio-acumuladas-graph.html'
})
export class UnidadesFrioAcumuladasGraphComponent {

  @Input() data: any;
  @Input() servicio: Service;
  @ViewChild('grafico') graficoRef: ElementRef;
  
  par = [];
  constructor() {

  }

  ngOnInit() {
    this.crearGrafico();
  }

  crearGrafico() {
    // var ctx = document.getElementById("myChart").getContext('2d');
    this.organizarData();
    const ctx = this.graficoRef.nativeElement;
    const myChart = new Chart(ctx, {
      type: 'horizontalBar',
      options: {
        // scales: {
        //   xAxes: [{
        //     type: "time",
        //     time: {
        //       parser: 'DD-MM-YYYY',
        //       tooltipFormat: 'll'
        //     },
        //     scaleLabel: {
        //       display: true,
        //       labelString: 'Fechas'
        //     }
        //   }]
        // },
        responsive: true,
        title: {
          display: true,
          text: this.servicio.getName()
        },
        legendCallback: (chart) => {
          console.log('legendCallback', chart);
          // Return the HTML string here.
          return '<p>la locura</p>'
      }
        
      },
      data: {
        datasets: [
          {
            label: 'Resumen',
            data: [50]
          }
        ],
        labels: [this.par[0].y]
      }
    });
  }

  organizarData() {
    _.forEach(this.data.values, item => {
      const d = moment(item.fecha, 'YYYYMMDD').format('DD-MM-YYYY');
      this.par = [{x: item.avance_porcentual, y: d}];
    });
    console.log(this.par);
  }

}
