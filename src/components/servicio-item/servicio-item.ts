import { Component, Input } from '@angular/core';
import { Service } from '../../models/service';
import { ServicesProvider } from '../../providers/services/services.provider';
import { NavController } from 'ionic-angular';
import { GraphPage } from '../../pages/graph/graph';
import { LoadingProvider } from '../../providers/loading/loading';

/**
 * Generated class for the ServicioItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'servicio-item',
  templateUrl: 'servicio-item.html'
})
export class ServicioItemComponent {
  @Input() servicio: Service;

  constructor(private sp: ServicesProvider,
  private navCtrl: NavController,
private loading: LoadingProvider) {

  }

  verServicio() {
    this.loading.showLoading();
    this.sp.getService(this.servicio.getUuid())
    .subscribe(response => {
      this.loading.hideLoading();
      console.log('verServicio response', response);
      this.navCtrl.push(GraphPage, {data: response, servicio: this.servicio});
    },
  err => console.log('error', err));
  }

}
