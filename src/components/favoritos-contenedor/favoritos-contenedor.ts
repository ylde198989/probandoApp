import { Component } from '@angular/core';

/**
 * Generated class for the FavoritosContenedorComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'favoritos-contenedor',
  templateUrl: 'favoritos-contenedor.html'
})
export class FavoritosContenedorComponent {

  text: string;

  constructor() {
    console.log('Hello FavoritosContenedorComponent Component');
    this.text = 'Hello World';
  }

}
