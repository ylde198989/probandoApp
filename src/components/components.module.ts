import { NgModule } from '@angular/core';
import { FavoritosContenedorComponent } from './favoritos-contenedor/favoritos-contenedor';
import { FavoritoItemComponent } from './favorito-item/favorito-item';
import { ServicioItemComponent } from './servicio-item/servicio-item';
import { IonicApp, IonicModule } from 'ionic-angular';
import { ThingItemComponent } from './thing-item/thing-item';
import { UnidadesFrioGraphComponent } from './unidades-frio-graph/unidades-frio-graph';
import { PorcionesFrioGraphComponent } from './porciones-frio-graph/porciones-frio-graph';
import { UnidadesFrioAcumuladasGraphComponent } from './unidades-frio-acumuladas-graph/unidades-frio-acumuladas-graph';
@NgModule({
    declarations: [FavoritosContenedorComponent,
        FavoritoItemComponent,
        ServicioItemComponent,
        ThingItemComponent,
        UnidadesFrioGraphComponent,
    PorcionesFrioGraphComponent,
    UnidadesFrioAcumuladasGraphComponent],
    bootstrap: [IonicApp],
    imports: [IonicModule],
    exports: [FavoritosContenedorComponent,
        FavoritoItemComponent,
        ServicioItemComponent,
        ThingItemComponent,
        UnidadesFrioGraphComponent,
    PorcionesFrioGraphComponent,
    UnidadesFrioAcumuladasGraphComponent]
})
export class ComponentsModule { }
