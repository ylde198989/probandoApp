import { Component, Input, ViewChild, ElementRef, OnInit } from '@angular/core';
import * as _ from 'lodash';
import moment from 'moment';
import Chart from 'chart.js';
import { Service } from '../../models/service';
/**
 * Generated class for the UnidadesFrioGraphComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'porciones-frio-graph',
  templateUrl: 'porciones-frio-graph.html'
})
export class PorcionesFrioGraphComponent implements OnInit {
  @Input() data: any;
  @Input() servicio: Service;
  @ViewChild('grafico') graficoRef: ElementRef;
  lineData = [];
  barData = [];
  dates = [];
  constructor() {

  }

  ngOnInit() {
    this.crearGrafico();
  }

  crearGrafico() {
    // var ctx = document.getElementById("myChart").getContext('2d');
    this.organizarData();
    const ctx = this.graficoRef.nativeElement;
    const myChart = new Chart(ctx, {
      type: 'bar',
      options: {
        scales: {
          xAxes: [{
            type: "time",
            time: {
              parser: 'DD-MM-YYYY',
              tooltipFormat: 'll'
            },
            scaleLabel: {
              display: true,
              labelString: 'Fechas'
            }
          }]
        },
        responsive: true,
        title: {
          display: true,
          text: this.servicio.getName()
        },
        
      },
      data: {
        datasets: [
          {
            label: 'Portions',
            data: this.lineData,
            fill: false,
            type: 'line',
            tension: 0,
            borderColor: 'blue'
          },
          {
            label: 'Inter E',
            data: this.barData,
            backgroundColor: 'black'
          }
        ],
        labels: this.dates
      }
    });
  }

  organizarData() {
    _.forEach(this.data.values, item => {
      const d = moment(item.fecha, 'YYYYMMDD').format('DD-MM-YYYY');
      // this.barData.push({ x: d, y: item.diario });
      // this.lineData.push({ x: d, y: item.acumulado });

      this.dates.push(d);
      this.barData.push(item.inter_E);
      this.lineData.push(item.portions);
    });
    console.log(this.lineData, this.barData);
  }

}
