import { Component } from '@angular/core';

/**
 * Generated class for the FavoritoItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'favorito-item',
  templateUrl: 'favorito-item.html'
})
export class FavoritoItemComponent {

  text: string;

  constructor() {
    console.log('Hello FavoritoItemComponent Component');
    this.text = 'Favorito Item';
  }

}
