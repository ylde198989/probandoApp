import { Component, Input } from '@angular/core';
import { Thing } from '../../models/thing';
import { NavController } from 'ionic-angular';
import { DefinirCoordenadasPage } from '../../pages/definir-coordenadas/definir-coordenadas';

/**
 * Generated class for the ThingItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'thing-item',
  templateUrl: 'thing-item.html'
})
export class ThingItemComponent {
@Input() thing: Thing;

  constructor(public navCtrl: NavController) {
  }

  preSetGPS() {
    console.log('Comenzar con el seteo de las coordenadas');
    this.navCtrl.push(DefinirCoordenadasPage, {thing: this.thing});
  }

}
