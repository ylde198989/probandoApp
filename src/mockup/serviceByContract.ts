export const serviceByContractMockup = {
    "IDContrato": 1300,
    "Nombre_Contrato": "Parcela demostrativa El Pedrero",
    "services": [
        {
            "alarms": [
                {
                    "enabled": 1,
                    "evaluation_var": "Temperatura",
                    "id": 3,
                    "nombre": "escalamiento default"
                }
            ],
            "descrip": "Calculo de Chillings Unit, metodo Richarson (Utah)",
            "enabled": 1,
            "evaluation_vars": [
                "Temperatura"
            ],
            "id": 400,
            "is_favorite": 0,
            "max_things_allowed": 1,
            "name": "Unidades de frio",
            "parameters": [
                {
                    "name": "variedad",
                    "type": "String",
                    "value": "STAR"
                },
                {
                    "name": "horas_frio_CU",
                    "type": "String",
                    "value": "400"
                }
            ],
            "resolve_fx": "service_CU_Richarson",
            "stage_param": "default",
            "things": [
                "PSS001"
            ],
            "uuid": "baa82ca3-028e-4a16-9488-28be28b3e4d7"
        },
        {
            "alarms": [
                {
                    "enabled": 1,
                    "evaluation_var": "Temperatura",
                    "id": 3,
                    "nombre": "escalamiento default"
                }
            ],
            "descrip": "Calculo de Portion Unit, metodo Fishman (Dinamico)",
            "enabled": 1,
            "evaluation_vars": [
                "Temperatura"
            ],
            "id": 410,
            "is_favorite": 0,
            "max_things_allowed": 1,
            "name": "Porciones de frio",
            "resolve_fx": "service_CP_Fishman",
            "stage_param": "default",
            "things": [
                "PSS001"
            ],
            "uuid": "abd26f97-1721-42f9-b470-86e2b9a6c1ab"
        },
        {
            "alarms": [
                {
                    "enabled": 1,
                    "evaluation_var": "Temperatura",
                    "id": 3,
                    "nombre": "escalamiento default"
                }
            ],
            "descrip": "Resumen ejecutivo de Unidades de frío acumuladas en período",
            "enabled": 1,
            "evaluation_vars": [
                "Temperatura"
            ],
            "id": 420,
            "is_favorite": 0,
            "max_things_allowed": 1,
            "name": "Resumen ejecutivo",
            "parameters": [
                {
                    "name": "variedad",
                    "type": "String",
                    "value": "STAR"
                },
                {
                    "name": "horas_frio_CU",
                    "type": "int",
                    "value": "400"
                }
            ],
            "resolve_fx": "service_CU_Sumario",
            "stage_param": "default",
            "things": [
                "PSS001"
            ],
            "uuid": "974a3de5-72d7-4451-b81f-b1ab73591d3a"
        },
        {
            "alarms": [
                {
                    "enabled": 1,
                    "evaluation_var": "Temperatura",
                    "id": 3,
                    "nombre": "escalamiento default"
                }
            ],
            "descrip": "Calculo de Positive Chillings Unit, metodo Richarson (PCU)",
            "enabled": 1,
            "evaluation_vars": [
                "Temperatura"
            ],
            "id": 430,
            "is_favorite": 0,
            "max_things_allowed": 1,
            "name": "Unidades de frio POSITIVAS",
            "parameters": [
                {
                    "name": "variedad",
                    "type": "String",
                    "value": "STAR"
                },
                {
                    "name": "horas_frio_CU",
                    "type": "String",
                    "value": "400"
                }
            ],
            "resolve_fx": "service_CU_Richarson",
            "stage_param": "default",
            "things": [
                "PSS001"
            ],
            "uuid": "3dd5ea26-acdc-4140-9c2c-96a254d36135"
        }
    ],
    "things": [
        {
            "altura": 0,
            "descrip": "pss001",
            "latitud": 0,
            "longitud": 0,
            "nombre": "PSS001",
            "thing_code": "PSS001"
        }
    ]
}

const schema = {
    "type": "object",
    "properties": {
        "IDContrato": {

            "type": "integer"
        },
        "Nombre_Contrato": {

            "type": "string"
        },
        "services": {

            "type": "array",
            "items": {

                "type": "object",
                "properties": {
                    "alarms": {

                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "enabled": {

                                    "type": "integer"
                                },
                                "evaluation_var": {

                                    "type": "string"
                                },
                                "id": {

                                    "type": "integer"
                                },
                                "nombre": {

                                    "type": "string"
                                }
                            }
                        }
                    },
                    "descrip": {

                        "type": "string"
                    },
                    "enabled": {

                        "type": "integer"
                    },
                    "evaluation_vars": {

                        "type": "array",
                        "items": {

                            "type": "string"
                        }
                    },
                    "id": {

                        "type": "integer"
                    },
                    "is_favorite": {

                        "type": "integer"
                    },
                    "max_things_allowed": {

                        "type": "integer"
                    },
                    "name": {

                        "type": "string"
                    },
                    "parameters": {

                        "type": "array",
                        "items": {

                            "type": "object",
                            "properties": {
                                "name": {

                                    "type": "string"
                                },
                                "type": {

                                    "type": "string"
                                },
                                "value": {

                                    "type": "string"
                                }
                            }
                        }
                    },
                    "resolve_fx": {

                        "type": "string"
                    },
                    "stage_param": {

                        "type": "string"
                    },
                    "things": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    },
                    "uuid": {
                        "type": "string"
                    }
                }
            }
        },
        "things": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "altura": {
                        "type": "integer"
                    },
                    "descrip": {
                        "type": "string"
                    },
                    "latitud": {
                        "type": "integer"
                    },
                    "longitud": {
                        "type": "integer"
                    },
                    "nombre": {
                        "type": "string"
                    },
                    "thing_code": {
                        "type": "string",
                    }
                }
            }
        }
    }

};