export class Parameters {
    name: string;
    type: string;
    value: string;
    constructor() {

    }
    setAll(params) {
        this.name = params.name;
        this.type = params.type;
        this.value = params.value;
        return this;
    }
}