export class Alarm {
    enabled: number;
    evaluation_var: string;
    id: number;
    nombre: string;
    constructor() {

    }

    setAll(params) : Alarm{
        this.enabled = params.enabled;
        this.evaluation_var = params.evaluation_var;
        this.id = params.id;
        this.nombre = params.nombre;
        return this;
    }
}