
export class Thing {

    altura: number;
    descrip: string;
    latitud: number;
    longitud: number;
    nombre: string;
    thing_code: string;
    constructor() {

    }

    setAll(params): Thing {
        try {
            this.altura = params.altura;
            this.descrip = params.descrip;
            this.latitud = params.latitud;
            this.longitud = params.longitud;
            this.nombre = params.nombre;
            this.thing_code = params.thing_code;
        } catch (e) {
            console.error(e);
        }
        return this;
    }

}