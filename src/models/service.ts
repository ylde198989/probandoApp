import { Alarm } from "./alarm";
import { Parameters } from "./parameters";

export class Service {
    private alarms: Alarm[] = []; 
    private parameters: Parameters[] = [];
    private evaluation_vars: string[];
    private things: string[];
    private descrip: string;
    private enabled: number;
    private id: number;
    private is_favorite: number;
    private max_things_allowed: number;
    private name: string;
    private resolve_fx: string;
    private stage_param: string;
    private uuid: string;

    constructor() {

    }

    setAll(params) : Service{
        this.descrip = params.descrip;
        this.enabled = params.enabled;
        this.evaluation_vars = params.evaluation_vars;
        this.things = params.things;
        this.id = params.id;
        this.is_favorite = params.is_favorite;
        this.max_things_allowed = params.max_things_allowed;
        this.name = params.name;
        this.resolve_fx = params.resolve_fx;
        this.stage_param = params.stage_param;
        this.uuid = params.uuid;
        this.setAlarms(params.alarms);
        if(params.parameters) {
            this.setParameters(params.parameters);
        }
        return this;
    }

    setAlarms(alarms: any[]) {
        for(let i = 0; i < alarms.length; i++){
            this.alarms.push((new Alarm()).setAll(alarms[i]));
        }
    }

    setParameters(parametersArray: any[]) {
            for(let i = 0; i < parametersArray.length; i++){
                this.parameters.push((new Parameters()).setAll(parametersArray[i]));
            }
    }

    getUuid() {
        return this.uuid;
    }

    getName() {
        return this.name;
    }

    getDescrip() {
        return this.descrip;
    }

    getResolveFx() {
        return this.resolve_fx;
    }

}