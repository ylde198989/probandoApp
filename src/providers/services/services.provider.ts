import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Const from '../../app/enviroment.globals';
import { Service } from '../../models/service';
import * as _ from 'lodash';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServicesProvider {

  allServices: Service[] = [];
  favorites: Service[] = [];
  services: Service[] = [];

  constructor(public http: HttpClient) {
    console.log('Hello ServicesProvider Provider');
  }

  servicesByContract(idContract: number) {
    let p = new HttpParams().set('idContrato', '1300');
    return this.http.get(Const.url + '/servicesbycontract2', {params: p});
  }
  
  getService(uuid: string) {
    return this.http.post(Const.url + '/processservicedata', {uuid: uuid}, {});
  }

  setAllServices(services: any[]) {
    let f = _.filter(services, {is_favorite: 1});
    let s = _.filter(services, {is_favorite: 0});
    this.setFavorites(f);
    this.setServices(s);
    this.allServices.concat(this.favorites).concat(this.services);
  }

  setFavorites(f: any[]) {
    for(let i = 0; i < f.length; i++) {
      this.favorites.push((new Service()).setAll(f[i]));
    }
  }

  setServices(s: any) {
    for(let i = 0; i < s.length; i++) {
      this.services.push((new Service()).setAll(s[i]));
    }
  }
}
