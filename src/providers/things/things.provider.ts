import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Thing } from '../../models/thing';

/*
  Generated class for the ThingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ThingsProvider {
  things: Thing[] = [];
  constructor(public http: HttpClient) {
    console.log('Hello ThingsProvider Provider');
  }

  setAllThings(things: any[]) {
    for(let i = 0; i < things.length; i++) {
      this.things.push((new Thing()).setAll(things[i]));
    }
  }
}
