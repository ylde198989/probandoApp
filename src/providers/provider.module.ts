import { NgModule } from '@angular/core';
import { ServicesProvider } from './services/services.provider';
import { HttpClientModule } from '@angular/common/http';
import { ThingsProvider } from './things/things.provider';
import { LoadingProvider } from './loading/loading';

@NgModule({
  imports: [
    HttpClientModule
  ],
  providers: [
    ServicesProvider,
    ThingsProvider,
    LoadingProvider
  ]
})
export class ProviderModule { }
