const model = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "title": "ServicesByContractOutputModel",
    "type": "object",
    "properties": {
          "agromath_error":{
            "type":"object",
            "properties":{
                "code":{"type":"integer"},
                "description":{"type":"string"},
                "source":{"type":"string"}
            }
            },
          "IDContrato":{"type":"integer"},
          "Nombre_Contrato":{"type":"string"},
          "services":{
              "type":"array",
              "items":{
                  "type":"object",
                  "properties":{
                      "id":{"type":"integer"},
                      "name":{"type":"string"}
                  }
              }
          },
          "things":{
              "type":"array",
              "items":{
                  "type":"object",
                  "properties":{
                      "imei":{"type":"string"}
                  }
              }
          }
       }
   }
  