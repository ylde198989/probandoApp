import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraphPage } from './graph';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    GraphPage,
  ],
  imports: [
    IonicPageModule.forChild(GraphPage),
    ComponentsModule
  ],
  exports: [
    GraphPage
  ]
})
export class GraphPageModule {}
