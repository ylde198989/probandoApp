import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Service } from '../../models/service';

/**
 * Generated class for the GraphPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-graph',
  templateUrl: 'graph.html',
})
export class GraphPage {
  data: any;
  servicio: Service;
  tipo: number;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.data = this.navParams.get('data');
    this.servicio = this.navParams.get('servicio');
    console.log('service', this.servicio);
    
  }

  ionViewDidLoad() {
    this.selectGraph();
  }

  selectGraph() {
    switch(this.servicio.getResolveFx()) {
      case 'service_CU_Richarson':
      this.tipo = 1;
      break;
      case 'service_CP_Fishman':
      this.tipo = 2;
      break;
      case 'service_CU_Sumario':
      this.tipo = 3;
      break;
      default:
        console.error('No hay una funcion manejadora en el servicio');
      break;
    }
  }

}
