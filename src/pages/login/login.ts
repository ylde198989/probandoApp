import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { OlvidarContrasenaPage } from '../olvidar-contrasena/olvidar-contrasena';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	email:String='';
	password:String='';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  login(email, password) {
    let body={
    	email:this.email,
    	password:this.password
    }
    this.navCtrl.push(TabsPage);
    console.log(body);
  }

  olvido(){
    this.navCtrl.push(OlvidarContrasenaPage);
  }

}
