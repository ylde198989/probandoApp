import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services.provider';
import { serviceByContractMockup } from '../../mockup/serviceByContract';
import * as _ from 'lodash';
import { Thing } from '../../models/thing';
import { Service } from '../../models/service';
import { ThingsProvider } from '../../providers/things/things.provider';
import { LoadingProvider } from '../../providers/loading/loading';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../popover/popover';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  favorites: Service[] = [];
  services: Service[] = [];
  things: Thing[] = [];
  
  constructor(public navCtrl: NavController,
    public sp: ServicesProvider,
    public ts: ThingsProvider,
    public loading: LoadingProvider,
    public popoverCtrl: PopoverController) {
    this.loadContract();
  }

  loadContract() {
    // Por ahora asi
    // this.setData();
    this.loading.showLoading();
    this.sp.servicesByContract(1300)
    .subscribe(response => {
      this.setData(response);
      this.loading.hideLoading();
    },
    err => console.log(err));
  }

  setData(response?) {
    this.sp.setAllServices(serviceByContractMockup.services);
    this.favorites = this.sp.favorites;
    this.services = this.sp.services;

    this.ts.setAllThings(serviceByContractMockup.things);
    this.things = this.ts.things;

    console.log(this.favorites, this.services, this.things);
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    this.loading.showLoading();
    this.sp.servicesByContract(1300)
    .subscribe(response => {
      this.setData(response);
      this.loading.hideLoading();
      refresher.complete();
    },
    err => console.log(err));
  }


  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }



}
