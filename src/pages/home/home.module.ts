import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { HomePage } from './home';
import { ComponentsModule } from '../../components/components.module';
import { ProviderModule } from '../../providers/provider.module';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    ComponentsModule,
    ProviderModule
  ],
  exports: [
    HomePage
  ]
})
export class HomePageModule { }
