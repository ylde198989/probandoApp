import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { NotificacionesPage } from '../notificaciones/notificaciones';
import { ServiciosPage } from '../servicios/servicios';
import { DispositivosPage } from '../dispositivos/dispositivos';
import { UbicacionPage } from '../ubicacion/ubicacion';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = NotificacionesPage;
  tab3Root = ServiciosPage;
  tab4Root = DispositivosPage;
  tab5Root = UbicacionPage;

  constructor() {

  }
}
