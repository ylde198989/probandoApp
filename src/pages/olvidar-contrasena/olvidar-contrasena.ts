import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';

/**
 * Generated class for the OlvidarContrasenaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-olvidar-contrasena',
  templateUrl: 'olvidar-contrasena.html',
})
export class OlvidarContrasenaPage {

	email:String='';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

   recuperar(email) {
    let body={
    	email:this.email
    }
    this.navCtrl.push(LoginPage);
    console.log(body);
  }
  salir() {
    this.navCtrl.push(LoginPage);
  }

}
