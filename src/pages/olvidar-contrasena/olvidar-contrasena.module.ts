import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OlvidarContrasenaPage } from './olvidar-contrasena';

@NgModule({
  declarations: [
    OlvidarContrasenaPage,
  ],
  imports: [
    IonicPageModule.forChild(OlvidarContrasenaPage),
  ],
})
export class OlvidarContrasenaPageModule {}
