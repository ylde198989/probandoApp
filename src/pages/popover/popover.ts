import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ContactenosPage } from '../contactenos/contactenos';
import { NosotrosPage } from '../nosotros/nosotros';
@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  contacto() {
    this.navCtrl.push(ContactenosPage);
  }

  nosotros(){
  	this.navCtrl.push(NosotrosPage);
  }

}
