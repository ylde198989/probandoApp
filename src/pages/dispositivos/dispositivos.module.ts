import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DispositivosPage } from './dispositivos';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DispositivosPage,
  ],
  imports: [
    IonicPageModule.forChild(DispositivosPage),
    ComponentsModule
  ],
  exports: [
    DispositivosPage
  ]
})
export class DispositivosPageModule {}
