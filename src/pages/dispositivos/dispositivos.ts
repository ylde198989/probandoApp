import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ThingsProvider } from '../../providers/things/things.provider';
import { Thing } from '../../models/thing';

/**
 * Generated class for the DispositivosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-dispositivos',
 	templateUrl: 'dispositivos.html',
 })
 export class DispositivosPage {
 	things: Thing[] = [];
 	dispositivo:any[]=[{title:String, descripcion:String},
 	{title:String, descripcion:String},
 	{title:String, descripcion:String},
 	{title:String, descripcion:String},
 	{title:String, descripcion:String}];
 	constructor(public navCtrl: NavController, public navParams: NavParams, public ts: ThingsProvider) {
 	}

 	ionViewDidLoad() {
 		this.things = this.ts.things;
 		this.dispositivo=[{title:'Dispositivo 1', descripcion:'Variedad Star, Sector 5'},
 		{title:'Dispositivo 2', descripcion:'Variedad Star, Sector 2'},
 		{title:'Dispositivo 3', descripcion:'Variedad Star, Sector 3'},
 		{title:'Dispositivo 4', descripcion:'Variedad Star, Sector 4'},
 		{title:'Dispositivo 5', descripcion:'Variedad Star, Sector 1'}];
 	}

 }
