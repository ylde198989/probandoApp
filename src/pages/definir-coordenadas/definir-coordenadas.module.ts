import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DefinirCoordenadasPage } from './definir-coordenadas';
import { Geolocation } from '@ionic-native/geolocation';

@NgModule({
  declarations: [
    DefinirCoordenadasPage,
  ],
  imports: [
    IonicPageModule.forChild(DefinirCoordenadasPage),
  ],
  providers: [
    Geolocation
  ]
})
export class DefinirCoordenadasPageModule {}
