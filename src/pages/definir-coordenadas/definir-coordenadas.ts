import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  LatLng
} from '@ionic-native/google-maps';

import { Thing } from '../../models/thing';
/**
 * Generated class for the DefinirCoordenadasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-definir-coordenadas',
  templateUrl: 'definir-coordenadas.html',
})
export class DefinirCoordenadasPage {
  @ViewChild(Content) content: Content;
  @ViewChild('map') mapRef: ElementRef;
  map: GoogleMap;
  marker: Marker;
  thing: Thing;
  pre = {
    latitud: 0,
    longitud: 0,
    altitud: 0,
    accuracy: undefined
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private geolocation: Geolocation) {
    this.thing = this.navParams.get('thing');
  }

  ionViewDidLoad() {
    // console.log(this.thing);
    this.geolocation.getCurrentPosition().then(response => this.handlePosition(response));
    
  }

  handlePosition(response) {
    // console.log(response);
    this.compare(response.coords.latitude, response.coords.longitude, response.coords.altitud, response.coords.accuracy);
    this.showMap(response.coords.latitude, response.coords.longitude);

  }

  watch() {
    this.geolocation.watchPosition().subscribe(response => this.handlePosition(response));
  }

  compare(lat, lng, alt, acc) {
    if (lat !== this.pre.latitud) {
      console.log('cambio la latitud', lat, lng, alt, acc);
      this.setPre(lat, lng, alt, acc);
      return;
    } else if (lng !== this.pre.longitud) {
      console.log('cambio la longitud', lat, lng, alt, acc);
      this.setPre(lat, lng, alt, acc);
      return;
    } else if (alt !== this.pre.altitud) {
      console.log('cambio la altitud', lat, lng, alt, acc);
      this.setPre(lat, lng, alt, acc);
      return;
    } else if (acc !== this.pre.accuracy) {
      console.log('cambio la accuracy', lat, lng, alt, acc);
      this.setPre(lat, lng, alt, acc);
      return;
    }
  }

  setPre(latitude, longitude, altitude, accuracy) {
    this.pre = {
      latitud: latitude,
      longitud: longitude,
      altitud: altitude,
      accuracy: accuracy
    };
  }

  showMap(lat, lng) {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: lat,
          lng: lng
        },
        zoom: 18
      },
      
    };

    if (!this.map) {
      console.log('Creando el mapa');
      this.map = GoogleMaps.create(this.mapRef.nativeElement, mapOptions);
      this.map.one(GoogleMapsEvent.MAP_READY)
        .then(() => {
          console.log('El mapa esta listo');
          this.crearMarcador(lat, lng);

          // this.map.on(GoogleMapsEvent.MAP_DRAG_END).subscribe(
          //   () => {
          //     // console.log(this.map.getCameraPosition().target);
          //     if (this.marker) {
          //       this.marker.setPosition(this.map.getCameraPosition().target);
                
          //     }
          //   });
          this.watch();
        });
    } else {
      console.log('Map is ready! crearMarcador');
      this.map.setCameraTarget(new LatLng(lat, lng));
      this.crearMarcador(lat, lng);
    }
    this.content.resize();
  }

  crearMarcador(lat, lng) {
    if(!this.marker) {
      this.map.addMarker({
        title: 'Ionic',
        icon: 'blue',
        animation: 'DROP',
        position: {
          lat: lat,
          lng: lng
        },
        dragable: false
      })
        .then(marker => {
          this.marker = marker;
          return marker;
        });
    } else {
      this.marker.setPosition(new LatLng(lat, lng));
    }
  }

  reset() {
    this.geolocation.getCurrentPosition().then(response => this.handlePosition(response));
  }

}
