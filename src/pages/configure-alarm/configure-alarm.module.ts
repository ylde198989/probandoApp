import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfigureAlarmPage } from './configure-alarm';

@NgModule({
  declarations: [
    ConfigureAlarmPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfigureAlarmPage),
  ],
  exports: [
    ConfigureAlarmPage
  ]
})
export class ConfigureAlarmPageModule {}
