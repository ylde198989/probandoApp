import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  MarkerIcon
} from '@ionic-native/google-maps';

import { Thing } from '../../models/thing';
import { ThingsProvider } from '../../providers/things/things.provider';
import _ from 'lodash';

declare var google:any;

@IonicPage()
@Component({
  selector: 'page-ubicacion',
  templateUrl: 'ubicacion.html',
})
export class UbicacionPage {
  @ViewChild(Content) content: Content;
  @ViewChild('map') mapRef: ElementRef;
   // map: GoogleMap;
   // marker: Marker;
   // things: Thing[];
   
   constructor(public navCtrl: NavController, public navParams: NavParams,
     public tp: ThingsProvider) {
   }

   ionViewDidLoad() {
     this.showMap();
     //this.things = this.tp.things;
     //this.showMap(this.things[0].latitud, this.things[0].longitud);
   }

   showMap(){
     const coords=[{location:new google.maps.LatLng(-26.7366368,-70.5229184)},
     {location:new google.maps.LatLng(-23.7366368,-70.5229184)},
     {location:new google.maps.LatLng(-28.7366368,-70.5229184)}];
     // const location= new google.maps.LatLng(-26.7366368,-70.5229184);
     // const location2= new google.maps.LatLng(-26.2467421,-69.6366111,15);

     const options={
       center: coords[0].location,
       draggable: true,
       animation: google.maps.Animation.DROP,
       zoom:5,
       style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
       mapTypeIds: ['roadmap', 'terrain']
     };

     const map= new google.maps.Map(this.mapRef.nativeElement, options);

    for (var i = 0; i < coords.length; ++i) {
      this.addMarker(coords[i].location, map);
    }

     // this.addMarker(coords[0].location,  map);
     // this.addMarker(coords[1].location,  map);
   }


   addMarker(position, map){

     let icon: MarkerIcon = {
       url: 'assets/imgs/iconoubicacion.png',
       size: {
         width: 60,
         height: 82
       }
     };

     return new google.maps.Marker({
       position,
       map,
       icon:icon
     });
   }

   // showMap(lat, lng) {
   //   let mapOptions: GoogleMapOptions = {
   //     camera: {
   //       target: {
   //         lat: lat,
   //         lng: lng
   //       },
   //       zoom: 18
   //     }
   //   };

   //   if (!this.map) {
   //     this.map = GoogleMaps.create(this.mapRef.nativeElement, mapOptions);
   //     this.map.one(GoogleMapsEvent.MAP_READY)
   //     .then(() => {
   //       this.crearMarcadores();
   //     });
   //   } else {
   //     console.log('Map is ready!');

   //   }
   //   this.content.resize();
   // }

   // crearMarcadores() {

   //   _.forEach(this.things, thing => this.crearMarcador(thing.latitud, thing.longitud, thing.nombre));
   // }

   // crearMarcador(lat, lng, nombre) {

   //   let icon: MarkerIcon = {
   //     url: 'assets/imgs/iconoubicacion.png',
   //     size: {
   //       width: 32,
   //       height: 24
   //     }
   //   };

   //   this.map.addMarker({
   //     title: nombre,
   //     icon: icon,
   //     animation: 'DROP',
   //     position: {
   //       lat: lat,
   //       lng: lng
   //     },
   //     dragable: false
   //   })
   //   .then(marker => {
   //     marker.on(GoogleMapsEvent.MARKER_CLICK)
   //     .subscribe(() => {
   //       alert('clicked');
   //     });
   //   });

   // }

 }
