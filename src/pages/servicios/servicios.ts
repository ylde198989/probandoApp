import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DispositivosPage } from '../dispositivos/dispositivos';

/**
 * Generated class for the ServiciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-servicios',
 	templateUrl: 'servicios.html',
 })
 export class ServiciosPage {

 	

 	constructor(public navCtrl: NavController, public navParams: NavParams) {
 	}

 	configurar() {
 		this.navCtrl.push(DispositivosPage);
 	}

 }
