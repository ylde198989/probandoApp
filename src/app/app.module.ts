import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { GoogleMaps } from '@ionic-native/google-maps';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { OlvidarContrasenaPage } from '../pages/olvidar-contrasena/olvidar-contrasena';
import { NosotrosPage } from '../pages/nosotros/nosotros';
import { ContactenosPage } from '../pages/contactenos/contactenos';
import { PopoverPage } from '../pages/popover/popover';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ComponentsModule } from '../components/components.module';
import { HomePageModule } from '../pages/home/home.module';
import { ProviderModule } from '../providers/provider.module';
import { DispositivosPageModule } from '../pages/dispositivos/dispositivos.module';
import { NotificacionesPageModule } from '../pages/notificaciones/notificaciones.module';
import { UbicacionPageModule } from '../pages/ubicacion/ubicacion.module';
import { ServiciosPageModule } from '../pages/servicios/servicios.module';
import { DefinirCoordenadasPageModule } from '../pages/definir-coordenadas/definir-coordenadas.module';
import { GraphPageModule } from '../pages/graph/graph.module';
import { LoadingProvider } from '../providers/loading/loading';

@NgModule({
  declarations: [
  MyApp,
  TabsPage,
  LoginPage,
  OlvidarContrasenaPage,
  NosotrosPage,
  ContactenosPage,
  PopoverPage
  ],
  imports: [
  BrowserModule,
  IonicModule.forRoot(MyApp),
  ComponentsModule,
  HomePageModule,
  DispositivosPageModule,
  NotificacionesPageModule,
  UbicacionPageModule,
  ServiciosPageModule,
  ProviderModule,
  DefinirCoordenadasPageModule,
  GraphPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
  MyApp,
  TabsPage,
  LoginPage,
  OlvidarContrasenaPage,
  ContactenosPage,
  NosotrosPage,
  PopoverPage
  ],
  providers: [
  StatusBar,
  GoogleMaps,
  SplashScreen,
  {provide: ErrorHandler, useClass: IonicErrorHandler},
  LoadingProvider
  ]
})
export class AppModule {}
